<html>
<head>
  <meta charset="utf-8" />
  <title>Difusión Cultural</title>
  <link rel="stylesheet" href="css/estilo.css" />
</head>

<body>

<div id="contenido_tema">
<?php
include 'curl-util.php';

// Ojo, falta checar que $_GET['key'] sea un valor bueno, por seguridad. Que pasa si ponen un key falso?

$resp = json_decode(get_data($COUCHDB_URL . '/_design/tema/_view/detalle?startkey=["' . $_GET['key'] . '"]&endkey=["' . $_GET['key'] . '",{}]'), true);

$resp = $resp['rows'];
echo $resp[0]['value']['contenido'];
?>

<h3>Comentarios</h3><a name="comentarios"></a>

<div id="comentarios">

<?php
for ($i = 1; $i < sizeof($resp); $i++) {
    $comentario = $resp[$i]['value'];
    echo '<div class="comentario">' . $comentario['fecha'] . '<br />' . $comentario['contenido'] . '</div>';
}
?>
</div> <!-- comentarios -->

<form method="" id="nuevo_comentario" action="">
  <input type="hidden" name="tema_id" id="tema_id" value="<?= $_GET['key'] ?>" />
  <textarea name="comentario" id="comentario"></textarea>
  <br /><input type="submit" value="Enviar" />
</form>

</div>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/difusion/extras.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function() {

    var forma = {
        tema_id : {
            name : 'tema_id',
            selector : '#tema_id',
        },
        comentario : {
            name : 'comentario',
            selector : '#comentario',
            mensaje : "** No se ha definido ningun comentario",
        },
    }

    var forma_condiciones = {
        condiciones : {
            obligatorio : [forma.tema_id, forma.comentario, ],
            aviso       : [false, true],
            condiciones : [or, and],
        }
    }

    $( "#nuevo_comentario" ).submit(function( event ) {

        event.preventDefault();

        var resultadoForma = check(forma_condiciones.condiciones);

        if (resultadoForma) {
            var resJson = formaToJSonSensillo(forma);
            $.ajax({
                url : 'comentar.php',
                type : 'post',
                data : resJson,
                success : function (data, status){

                    var d = new Date();
                    var comentario = '<div class="comentario">' + d + '<br />' + $('#comentario').val() + '</div>';

                    $('#comentarios').append(comentario);
                    $('#comentario').val("");
                },
                error : function(xhr, desc, err) {
                    console.log(xhr);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            });
        }

    });

});// docummnet.ready();


</script>


</body>
</html>
