 {
    "temas" : {
        "tema_base" : {
            "obligatorio" : ["forma.titulo", "forma.resumen", "forma.imagen"],
            "aviso"       : [true, true, false],
            "condiciones" : ["and", "or", "or"],
            "sactivas"    : ["forma.imagen"]
        },
        "tema_video" : {
            "obligatorio" : ["forma.titulo", "forma.resumen", "forma.url_video"],
            "aviso"       : [true, false, true],
            "condiciones" : ["and", "or", "and"],
            "sactivas"    : ["forma.url_video"]
        },

        "tema_liga" : {
            "obligatorio" : ["forma.titulo", "forma.resumen", "forma.imagen", "forma.liga_externa" ],
            "aviso"       : [true, false, false, true],
            "condiciones" : ["and", "or", "or", "and"],
            "sactivas"    : ["forma.liga_externa", "forma.imagen"]
        },

        "tema_contenido" : {
            "obligatorio" : ["forma.titulo", "forma.resumen", "forma.imagen", "forma.contenido"],
            "aviso"       : [true, true, false, true],
            "condiciones" : ["and", "or", "or", "and"],
            "sactivas"    : ["forma.contenido", "forma.imagen"]
        }
    },

    "forma" : {

        "tipo_tema": {
            "name" : "tipo_tema",
            "selector": "input:radio['name'='radio_tipoTema']"
        },
        "titulo" : {
            "name" : "titulo",
            "selector" : "#titulo",
            "mensaje" : "** Hace falta un titulo"
        },
        "columnas" : {
            "name" : "columnas",
            "selector": "select['name'='columnas']"
        },
        "fondo_resumen" : {
            "name" : "fondo_resumen",
            "selector": "input['name='fondo_resumen']"
        },
        "controles" : {
            "name" : "controles"    ,
            "selector": "input['name'='controles']:checked",
            "opciones": true
        },
        "resumen" : {
            "name" : "resumen",
            "selector" : "resumen",
            "mensaje" : "** Falta incluir un resumen del tema",
            "tipoSelector" : 1
        },
        "imagen" : {
            "name" : "imagen"       ,
            "selector" : "#imagen",
            "mensaje" : "** No se ha seleccionado ninguna imagen",
            "contenedorID" : "#dimagen"
        },
        "url_video" : {
            "name" : "url_video"    ,
            "selector" : "#url_video",
            "mensaje" : "** Falta incluir la direccion del video",
            "contenedorID" : "#dvideo"
        },
        "liga_externa" : {
            "name" : "liga_externa" ,
            "selector" : "#liga_externa",
            "mensaje" : "** Falta la liga",
            "contenedorID" : "#delink"
        },
        "contenido" : {
            "name" : "contenido"    ,
            "selector" : "contenido",
            "mensaje" : "** No se ha definido ningun contendio",
            "tipoSelector" : 1,
            "contenedorID" : "#dcontenido"
        }
    },
    "secciones" : [  "forma.imagen", "forma.liga_externa", "forma.contenido", "forma.url_video" ],
    "seccionesBasicas" : ["forma.columnas", "forma.fondo_resumen", "forma.controles"]
}
