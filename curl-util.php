<?php

include 'config.php';

// $COUCHDB_SERVER = 'http://127.0.0.1:5984/';
//$COUCHDB_DATABASE = 'vestibulo';
//$COUCHDB_URL = $COUCHDB_SERVER . $COUCHDB_DATABASE;



/* gets the data from a URL */
function get_data($url) {
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}


function post_json_data($url, $fields) {
    //url-ify the data for the POST
    $data_string = json_encode($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );

    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}
?>
