<?php
//header("Location: tema.php?key=" . $_POST['tema_id']);  // Redirect after post
include 'curl-util.php';

$fields = array(
    'tema' => $_POST['tema_id'],
    'type' => 'comentario',
    'fecha' => round(microtime(true) * 1000),   // milliseconds
    'contenido' => $_POST['comentario']
);

post_json_data($COUCHDB_URL, $fields);

?>