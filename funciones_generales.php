<?php

//include('confing.php');

function check($array, $name, $default){
    return isset($array[$name]) ? $array[$name] : $default ;
}

function checkGET($name){
    return  check($_GET, $name, false);
}

function checkGETdefault($name, $default){
    return check($_GET, $name, $default);
}

function checkPOST($name){
    return  check($_POST, $name, false);
}

function checkPOSTdefault($name, $default){
    return check($_POST, $name, $default);
}





?>