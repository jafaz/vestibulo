<?php
include '../config.php';
include '../curl-util.php';

$tema_key =  isset($_GET[$key]) ? $_GET[$key] : null;

if ($tema_key) {
    $tema_data = json_decode(get_data($COUCHDB_URL . '/' . $tema_key), true);
} else {
    $tema_data = array();
}

?>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Difusión Cultural</title>
    <link rel="stylesheet" href="../css/estilo.css" />
    <link rel="stylesheet" href="../js/colpick-jQuery-Color-Picker-master/css/colpick.css" type="text/css"/>
  </head>

<div id="agregar-tema" />

    <div id="header">
      <a href="../index.php" >Inicio </a> /
      <a href="index.php" >Lista de temas </a> /
      <a href="tema.php" >Agregar tema </a>
    </div>

    <h1> Agregar tema </h1>

  <body>

    <form id="nuevo_tema" name="nuevo_tema" method="" action="">

      <div ><input class="boton" id="cotrol_forma" type="submit" value="<?php echo ($tema_key ? 'Guardar cambios' : 'Agregar tema') ?>" /></div>

      <div id="tipo_tema">
        Tipo de tema:
        <label>
          <input type="radio" name="radio_tipoTema" id="tema_base" value="tema_base" checked/>
          Básica
        </label>
        <label>
          <input type="radio" name="radio_tipoTema" id="tema_video" value="tema_video" />
          Con video
        </label>
        <label>
          <input type="radio" name="radio_tipoTema" id="tema_liga" value="tema_liga" />
          Con pagina web externa
        </label>
        <label>
          <input type="radio" name="radio_tipoTema" id="tema_contenido" value="tema_contenido" />
          Con seccion de tema
        </label>
      </div>


      <div><label>
        Titulo
        <input type="text" name="titulo" id="titulo" />
        (para redes sociales)
      </label></div>

      <div>
        <label> Ancho tema </label>
        <select name="columnas">
          <option value="1" selected> 1 columna</option>
          <option value="2">2 columnas</option>
          <option value="3">3 columnas</option>
        </select>
      </div>

      <div>
        <label> Selecciona el de fondo </label>
        <input type="text" id="picker" name="fondo_resumen" value="#ffffff" />
      </div>

      <div>
        Botones:
        <label>
          <input type="checkbox" name="controles" value="like" id="like" />
          Me gusta
        </label>
        <label>
          <input type="checkbox" name="controles" value="dislike" id="dislike" />
        No me gusta
        </label>
        <label>
          <input type="checkbox" name="controles" value="share" id="share" />
          Compartir
        </label>
        <label>
          <input type="checkbox" name="controles" value="comentarios" id="comentarios" />
          Comentarios
        </label>
      </div>


    <div id="dimagen" class="dimagen">
    <label>Dirección de la imagen:
    <input type="input" name="imagen" id="imagen" />
    </label>

      </div>

      <div>
        <label> texto tema
        <textarea name="resumen" id="resumen" cols="45" rows="5"></textarea>
        </label>
      </div>




      <div id="dvideo">
        <label>Video (liga de Youtube)
        <input type="text" name="url_video" id="url_video" />
        </label>
      </div>



      <div id="delink">
        <label>Direccion web
        <input type="text" name="liga_externa" id="liga_externa" />
        </label>
      </div>



      <div id="dcontenido">
        <label>Texto Página
        <textarea name="contenido" id="contenido" cols="45" rows="5"></textarea>
        </label>
      </div>

    </form>

</div> <!-- agragar tema -->

  <script src="../js/jquery-1.11.1.min.js"></script>
  <script src="../js/tinymce/tinymce.min.js"></script>
  <script src="../js/colpick-jQuery-Color-Picker-master/js/colpick.js" type="text/javascript"></script>

  <script src="../js/difusion/extras.js" type="text/javascript"></script>



<script type="text/javascript">
$(document).ready(function() {
var tema_key = '<?php echo $tema_key ?>';

$('.forma_aviso').hide();

$('#delink').hide();
$('#dcontenido').hide();
$('#dvideo').hide();

$('input:radio[name="radio_tipoTema"]').change(function(){

    //cuando se cambia el template del tema se quitan los mensajes
    //de error de la forma
    $(slectorError).remove();

    var tema = temas[ $(this).val()];
    activaSeccion(tema.sactivas, secciones);

});



var forma = {
    // variable : idHTML, selectroJquery, mensajeError

    'tipo_tema': {
        name : 'tipo_tema',
        selector: 'input:radio[name="radio_tipoTema"]',
        valor : '<?php echo ($tema_data['tipo_tema'] ? $tema_data['tipo_tema'] : 'tema_base') ?>',
        tipo:  'radio',
    },
    'titulo' : {
        name : 'titulo',
        selector : '#titulo',
        mensaje : '** Hace falta un titulo',
        valor : <?php
        if ($tema_data['titulo']) {
            echo "'" . addslashes($tema_data['titulo']) . "'";
        } else {
            echo 'null';
        } ?>,
        tipo : 'input',
    },
    'columnas' : {
        name : 'columnas'     ,
        selector: 'select[name="columnas"]',
        valor : <?php echo ($tema_data['columnas'] ? $tema_data['columnas'] : 1) ?>,
        tipo : 'select',
    },
    'fondo_resumen' : {
        name : 'fondo_resumen',
        selector: 'input[name="fondo_resumen"]',
        valor : '<?php echo ($tema_data['fondo_resumen'] ? $tema_data['fondo_resumen'] : 'ffffff') ?>',
        tipo : 'input',
    },
    'controles' : {
        name : 'controles'    ,
        selector: 'input[name=controles]:checked',
        opciones: true,
        valor : <?php
        if ($tema_data['controles']) {
            echo json_encode($tema_data['controles']);
        } else {
            echo '["like", "dislike"]';
        } ?>,
        tipo : 'checkbox',
    },
    'resumen' : {
        name : 'resumen',
        selector : 'resumen',
        mensaje : '** Falta incluir un resumen del tema',
        tipoSelector : 1,
        valor : '<?php echo ($tema_data['resumen'] ? implode("\\n", explode("\n", addslashes($tema_data['resumen']))) : '') ?>',
        tipo : 'tiny',
    },
    'imagen' : {
        name : 'imagen'       ,
        selector : '#imagen',
        mensaje : '** No se ha seleccionado ninguna imagen',
        contenedorID : "#dimagen",
        valor : <?php
        if ($tema_data['imagen']) {
            echo "'" . $tema_data['imagen'] . "'";
        } else {
            echo 'null';
        } ?>,
        tipo : 'input',
    },
    'url_video' : {
        name : 'url_video'    ,
        selector : '#url_video',
        mensaje : '** Falta incluir la direccion del video',
        contenedorID : "#dvideo",
        valor : <?php
        if ($tema_data['url_video']) {
            echo "'" . $tema_data['url_video'] . "'";
        } else {
            echo 'null';
        } ?>,
        tipo : 'input',
    },
    'liga_externa' : {
        name : 'liga_externa' ,
        selector : '#liga_externa',
        mensaje : '** Falta la liga',
        contenedorID : "#delink",
        valor : <?php
        if ($tema_data['liga_externa']) {
            echo "'" . $tema_data['liga_externa'] . "'";
        } else {
            echo 'null';
        } ?>,
        tipo : 'input',
    },
    'contenido' : {
        name : 'contenido'    ,
        selector : 'contenido',
        mensaje : '** No se ha definido ningun contendio',
        tipoSelector : 1,
        contenedorID : "#dcontenido",
        valor : '<?php echo ($tema_data['contenido'] ? implode("\\n", explode("\n", addslashes($tema_data['contenido']))) : '') ?>',
        tipo : 'tiny',
    }
}


tinymce.init({
    id: "resumen",
    selector: "#resumen",
    theme: "modern",
    width: 800,
    height: 200,
    language : 'es',
       plugins: ["textcolor","link","image","paste","pagebreak",
              "table","media","fullscreen","lists"],
    toolbar: ["forecolor backcolor  undo redo  bold italic  alignleft aligncenter alignright pastetext pagebreak hr  fullscreen  fontselect  fontsizeselect"],
    content_css : "../css/tinymce.css",
    setup: function(ed) {
        ed.on('init', function(ed) {
            // valor inicial
            tinymce.get('resumen').setContent(forma.resumen.valor);

            $('#picker').colpick({
                layout:'hex',
                submit:0,
                colorScheme:'dark',
                onChange:function(hsb,hex,rgb,el,bySetColor) {
                    $(el).css('border-color','#'+hex);
                    // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                    if(!bySetColor) $(el).val(hex);

                    $(tinymce.get('resumen').getBody()).css("background-color",'#' + hex);
                }
            }).keyup(function(){
                $(this).colpickSetColor(this.value);
            }).colpickSetColor(forma.fondo_resumen.valor, true);
        });
    }
 });


tinymce.init({
    language : 'es',
    selector: "#contenido",
    theme: "modern",
    width: 800,
    height: 200,
    fontsize_formats: "8pt 9pt 10pt 11pt 12pt 26pt 36pt",
 plugins: ["textcolor","link","image","paste","pagebreak",
              "table","media","fullscreen","preview","lists"],
    toolbar: ["forecolor backcolor link unlink undo redo styleselect bold italic link image alignleft aligncenter alignright pastetext ","pagebreak hr media inserttable tableprops deletetable cell row column fullscreen preview fontselect  fontsizeselect bullist,numlist"],
    setup: function(ed) {
        ed.on('init', function(ed) {
            tinymce.get('contenido').setContent(forma.contenido.valor);
        });
    }
 });

for (var llaveCampo in forma) {
    var campo = forma[llaveCampo];
    if (!campo.valor) continue;

    switch (campo.tipo) {
        case 'input':
        $(campo.selector).val(campo.valor);
        break;

        case 'radio':
        $('#' + campo.valor).prop('checked', true);
        break;

        case 'select':
        $('select[name="' + campo.name + '"] option[value="' + campo.valor + '"]').prop("selected", true);
        break;

        case 'checkbox':
        for (var i in campo.valor) {
            $('input[name="' + campo.name + '"]:checkbox[value="' + campo.valor[i] + '"]').prop("checked", true);
        }
        break;

        default: break;
    }
}




/*
$.getJSON('../js/difusion/conf.js',function(data){
    console.log('success');
    p(data.forma);
    var conf = data;
}).error(function(e,t,s){
    console.log('error'+e+t+s);
});
*/



var secciones = [  forma.imagen, forma.liga_externa, forma.contenido, forma.url_video ];
var seccionesBasicas = [forma.columnas, forma.fondo_resumen, forma.controles];
//var seccionesBasicas = [forma.controles];

var temas = {
    'tema_base' : {
        obligatorio : [forma.titulo, forma.resumen, forma.imagen],
        aviso       : [true, true, false],
        condiciones : [and, or, or],
        sactivas    : [forma.imagen],
    },
    'tema_video' : {
        obligatorio : [forma.titulo, forma.resumen, forma.url_video],
        aviso       : [true, false, true],
        condiciones : [and, or, and],
        sactivas    : [forma.url_video],
    },

    'tema_liga' : {
        obligatorio : [forma.titulo, forma.resumen, forma.imagen, forma.liga_externa, ],
        aviso       : [true, false, false, true],
        condiciones : [and, or, or, and],
        sactivas    : [forma.liga_externa, forma.imagen]
    },

    'tema_contenido' : {
        obligatorio : [forma.titulo, forma.resumen, forma.imagen, forma.contenido],
        aviso       : [true, true, false, true],
        condiciones : [and, or, or, and],
        sactivas    : [forma.contenido, forma.imagen]
    },
}



$( "#nuevo_tema" ).submit(function( event ) {
    event.preventDefault();

    var tema_seccion = $('input:radio[name="radio_tipoTema"]:checked').val();
    var resultadoForma = check(temas[tema_seccion]);

    if ( resultadoForma ){

        var resJson = formaToJSon1(temas[tema_seccion].obligatorio, seccionesBasicas);
        <?php if ($tema_key) echo "resJson['_id'] = '$tema_key';" ?>
        <?php if ($tema_key) echo 'resJson["_rev"] = \'' . $tema_data["_rev"] . '\';' ?>
        <?php if ($tema_key) echo "resJson['pos'] = '" . $tema_data["pos"] . "';" ?>

        resJson['tipo_tema'] =  tema_seccion;
        resJson['type'] =  'tema';
        //p(resJson);
        $.ajax({
            url : 'addtema.php',
            type : 'post',
            data : resJson,
            success : function (data, status){
                alert('Tu tema ha sido guardado exitosamente.');
                window.location = 'index.php'
            },
            error : function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });

        //$.post("like.php", {"tema_id": tema_id});
    }else{
       // p(resultadoForma);
    }
   // p(temas[tema_seccion]);


});

activaSeccion(temas[forma.tipo_tema.valor].sactivas, secciones);

//tinymce.get('resumen').setContent('fsdfsdf');
//p(tinymce.get('resumen').getContent())
//asiganaValores(forma);
//p(forma);

//p(tinymce.get(selector).getContent());
t = tinymce;
//p(t1);
});
</script>


</body>
</html>
