<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  <head>
    <meta charset="utf-8" />
    <title>Difusión Cultural</title>
    <link rel="stylesheet" href="../js/jquery-ui-1.11.0.custom/jquery-ui.min.css" />
    <link rel="stylesheet" href="../css/estilo.css" />
  </head>

  <body>

<div id="lista-temas" />

 <div id="header">
      <a href="../index.php" >Inicio </a> /
      <a href="index.php" >Lista de temas </a> /
      <a href="tema.php" >Agregar tema </a> /
    </div>

    <h1> Lista de temas </h1>





<?php
//error_reporting(-1);
//ini_set('display_errors', 'On');

include '../curl-util.php';
include '../config.php';

$pagina = isset($_GET['pagina']) ? $_GET['pagina'] : 1;
$siguiente_pagina = $pagina + 1;
$num_temas_anteiores = ($pagina - 1) * $NUM_PAG_TEMA;

$consulta = $COUCHDB_URL .
            "/_design/tema/_view/lista?descending=true".
           "&limit=" . $NUM_PAG_TEMA_ADMIN . "&" .
           "&skip=" . $num_temas_anteiores ;


//$resp = json_decode(get_data($COUCHDB_URL . "/_design/tema/_view/lista?descending=true&limit=".$NUM_PAG_TEMA_ADMIN."&skip=1"), true);

$resp = json_decode(get_data($consulta), true);

$resp = $resp['rows'];

//print_r($resp);
?>



<ul id="lista">
<?php foreach($resp as $tema): ?>

<?php
  $id = $tema['id'];
  $rev= $tema['value'][$revision];
  $titulo = $tema['value'][$titulo];
?>

  <li class="item" data-pos="<?php echo $tema['value']['pos'] ?>" data-key="<?php echo $id ?>">
    </span><a href="tema.php?<?php echo $key ?>=<?php echo $id  ?>" >Editar</a>
    <?php echo $tema['value']['titulo']?>
  </li>

<?php endforeach; ?>
</ul>

<nav id="page-nav">
   <a href="<?php echo $PAGINA_URL . '/admin/?pagina='. $siguiente_pagina?>"></a>
</nav>
<div class="boton siguientes"> <a href="">Descargar los siguientes <?php echo $NUM_PAG_TEMA_ADMIN?> temas</a></div>


<a href="tema.php"><div class="boton" > Agregar nuevo tema </div> </a>

    </div><!-- lista temas -->

  <script src="../js/jquery-1.11.1.min.js"></script>
  <script src="../js/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script>
  <script src="../js/jquery.infinitescroll.min.js"></script>
  <script src="../js/difusion/extras.js" type="text/javascript"></script>

<script type="text/javascript" >
$(document).ready(function() {

    $('#lista').sortable({
        stop: function(event, ui) {
            var inf = Number(ui.item.next().data('pos'));
            var sup = Number(ui.item.prev().data('pos'));
            var pos;
            p(inf);
            p(sup);
            p(pos);
            if (!inf) inf = 0;
            if (!sup) pos = Math.ceil(inf) + 1
            if (!pos) pos = inf + ((sup - inf) / 2);

            ui.item.data('pos', pos);

            dJson = {'key': ui.item.data('key'),
                     'pos': pos}
            p(inf);
            p(sup);
            p(pos);
            p(dJson);
            $.ajax({
                url : 'reordena_tema.php',
                type : 'post',
                data : dJson,
                success : function (data, status){
                    // p(data)
                },
                error : function(xhr, desc, err) {
                    p('error');
                    console.log(xhr);
                    console.log("Details: " + desc + "\nError:" + err);
                }
            });
        },
    });

    $('#lista').disableSelection();

    $('#lista').infinitescroll({
        debug : true,
        navSelector  : '#page-nav',    // selector for the paged navigation
        nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
        itemSelector : '.item',     // selector for all items you'll
        // retrieve
        loadingText  : 'Descargando temas...',
        loading: {
            finishedMsg: 'No hay mas temas por descargar',
            img: '<?php echo $PAGINA_URL ?>' + '/img/loading.gif',
        },

    });

   $('.siguientes').click(function(){
       p("hola");
       $('#lista').infinitescroll('retrieve');
       ;return false;
   });


});
</script>
  </body>
</html>
