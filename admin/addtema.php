<?php
error_reporting(-1);
ini_set('display_errors', 'On');

include '../curl-util.php';
include '../config.php';

if ($_POST) {
    $fields = $_POST;
    $fields['type'] = 'tema';

    $tema_key = isset($_POST['_id']) ? $_POST['_id'] : null;

    // Obtenemos el tema con posicion mas alta para asignarle al nuevo
    // tema la siguiente posicion
    if (is_null($tema_key)) {
        $last_doc = json_decode(get_data($COUCHDB_URL . "/_design/tema/_view/lista?descending=true&limit=1"), true);
        $last_doc = $last_doc['rows'][0];
        $fields['pos'] = "" . (ceil((float)$last_doc['value']['pos']) + 1);
    }

    echo post_json_data($COUCHDB_URL, $fields);
}

?>
