<?php
ini_set('display_errors', 'On');
require_once "Mail.php";
include('config.php');
include('funciones_generales.php');

$usuario_nombre = checkPOST($email_nombre);
$usuario_email = checkPOST($email);
$content = checkPOST($email_mensaje);
$subjerct = checkPOSTdefault($email_tema, "");


$headers = array(
    'From'    => $EMAIL_FROM,
    'To'      => $EMAIL_TO,
    'Subject' => $EMAIL_TEMA ." ". $subjerct
    );

$smtp = Mail::factory('smtp', array(
    'host' => $EMAIL_HOST,
    'port' => $EMAIL_PORT,
    'auth' => true,
    'username' => $EMAIL_USERNAME,
    'password' => $EMAIL_PASSWORD
));

$body =
    "Nombre: ". $usuario_nombre . "\n" .
    "Correo electronico: " .$usuario_email. "\n".
    "Tema: ". $subjerct . "\n" .
    "Comentario: " . $content;

$mail = $smtp->send($EMAIL_TO, $headers, $body);

if (PEAR::isError($mail)) {
    //echo("<p>" . $mail->getMessage() . "</p>");
    echo "<p>Hubo un problema con el envio de su mensaje, por favor intente mas tarde</p>";
} else {
    echo"<p>El mensaje se envio con exito! <br/> ¡Gracias!</p>";
}


?>